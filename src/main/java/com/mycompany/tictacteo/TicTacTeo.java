/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.tictacteo;

import java.util.Scanner;

/**
 *
 * @author KHANOMMECOM
 */
public class TicTacTeo {

    static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game!!");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    private static void inputRowCol() {
        System.out.println("Please input row and col :");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw()) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }

    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin() {
        if (checkVertical(table,currentPlayer,col)) {
            return true;
        } else if (checkHorizontal(table,currentPlayer,row)) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static void showWin() {
        showTable();
        System.out.println(">>>>" + currentPlayer + " Win<<<<");
    }

    private static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>>> Darw <<<<");
    }

    public static boolean checkVertical(char[][]table, char currentPlayer, int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][]table, char currentPlayer, int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) { // Arguments
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

}
