package com.mycompany.tictacteo;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */


import com.mycompany.tictacteo.TicTacTeo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author KHANOMMECOM
 */
public class TicTacTeo1Test {
    
    public TicTacTeo1Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O','-','-'},
                          {'O','-','-'},
                          {'O','-','-'}};
        char currentPlayer = 'O';
        int col=1;
        assertEquals(true,TicTacTeo.checkVertical(table,currentPlayer,col));
    }
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-','O','-'},
                          {'-','O','-'},
                          {'-','O','-'}};
        char currentPlayer = 'O';
        int col=2;
        assertEquals(true,TicTacTeo.checkVertical(table,currentPlayer,col));
    }
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-','-','O'},
                          {'-','-','O'},
                          {'-','-','O'}};
        char currentPlayer = 'O';
        int col=3;
        assertEquals(true,TicTacTeo.checkVertical(table,currentPlayer,col));
    }
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O','O','O'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char currentPlayer = 'O';
        int row=1;
        assertEquals(true,TicTacTeo.checkHorizontal(table,currentPlayer,row));
    }
    public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-','-','-'},
                          {'O','O','O'},
                          {'-','-','-'}};
        char currentPlayer = 'O';
        int row=2;
        assertEquals(true,TicTacTeo.checkHorizontal(table,currentPlayer,row));
    }
    public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-','-','-'},
                          {'-','-','-'},
                          {'O','O','O'}};
        char currentPlayer = 'O';
        int row=3;
        assertEquals(true,TicTacTeo.checkHorizontal(table,currentPlayer,row));
    }
}
